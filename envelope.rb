puts "How much total?"

total_val = gets.chomp.to_f

def getCurrencyAmount( total, type_of_currency )
   num_of_currency = (total / type_of_currency).to_i
   puts 'There are ' + (num_of_currency).to_s + ' ' + (type_of_currency).to_s + '\'s.'
   return num_of_currency
end

def getCurrencyRemaining( total, type_of_currency )
   remaining_total = total % type_of_currency
   puts "There is " + remaining_total.to_s + " remaining."
   return remaining_total
end

print "There are #{getCurrencyAmount( total_val, 20)}"
print " twenties in #{total_val},"
puts " with #{getCurrencyRemaining(total_val,20)} left over."

